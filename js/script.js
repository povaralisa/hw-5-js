"use strict";

function createNewUser() {
  let newUser = {
    name: prompt(`Введите имя`),
    lastName: prompt(`Введите фамилию`),
    birthday: prompt(`Введите дату рождения в формате (дд.мм.гг.)`),
    getLogin() {
      return (this.name[0] + this.lastName).toLowerCase();
    },
     getAge: function(){
            let yearNow = new Date();
            let yearAge = this.birthday.slice(6, 10);
            return yearNow.getFullYear() - yearAge;
            
        },
        getPassword: function(){
            return this.name.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        
        }
    }
        return newUser;

}

let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

